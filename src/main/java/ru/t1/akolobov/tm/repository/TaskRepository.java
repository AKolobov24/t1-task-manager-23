package ru.t1.akolobov.tm.repository;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.model.Task;

import java.util.List;
import java.util.stream.Collectors;

public final class TaskRepository extends AbstractUserOwnedRepository<Task> implements ITaskRepository {

    @Override
    @NotNull
    public List<Task> findAllByProjectId(@NotNull final String userId, @NotNull final String projectId) {
        return models.stream()
                .filter(t -> projectId.equals(t.getProjectId()) && userId.equals(t.getUserId()))
                .collect(Collectors.toList());
    }

}
