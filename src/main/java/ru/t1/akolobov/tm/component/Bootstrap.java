package ru.t1.akolobov.tm.component;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.repository.ICommandRepository;
import ru.t1.akolobov.tm.api.repository.IProjectRepository;
import ru.t1.akolobov.tm.api.repository.ITaskRepository;
import ru.t1.akolobov.tm.api.repository.IUserRepository;
import ru.t1.akolobov.tm.api.service.*;
import ru.t1.akolobov.tm.command.AbstractCommand;
import ru.t1.akolobov.tm.command.project.*;
import ru.t1.akolobov.tm.command.system.*;
import ru.t1.akolobov.tm.command.task.*;
import ru.t1.akolobov.tm.command.user.*;
import ru.t1.akolobov.tm.enumerated.Role;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.akolobov.tm.exception.system.CommandNotSupportedException;
import ru.t1.akolobov.tm.repository.CommandRepository;
import ru.t1.akolobov.tm.repository.ProjectRepository;
import ru.t1.akolobov.tm.repository.TaskRepository;
import ru.t1.akolobov.tm.repository.UserRepository;
import ru.t1.akolobov.tm.service.*;

import java.util.Scanner;

public final class Bootstrap implements IServiceLocator {

    @NotNull
    private final ICommandRepository commandRepository = new CommandRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ICommandService commandService = new CommandService(commandRepository);

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    @NotNull
    private final ILoggerService loggerService = new LoggerService();

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, projectRepository, taskRepository);

    @NotNull
    private final IAuthService authService = new AuthService(userService);

    {
        registry(new ApplicationAboutCommand());
        registry(new ApplicationExitCommand());
        registry(new ApplicationHelpCommand());
        registry(new ApplicationVersionCommand());
        registry(new ArgumentListCommand());
        registry(new CommandListCommand());
        registry(new SystemInfoCommand());

        registry(new ProjectChangeStatusByIdCommand());
        registry(new ProjectChangeStatusByIndexCommand());
        registry(new ProjectClearCommand());
        registry(new ProjectCompleteByIdCommand());
        registry(new ProjectCompleteByIndexCommand());
        registry(new ProjectCreateCommand());
        registry(new ProjectDisplayByIdCommand());
        registry(new ProjectDisplayByIndexCommand());
        registry(new ProjectListCommand());
        registry(new ProjectRemoveByIdCommand());
        registry(new ProjectRemoveByIndexCommand());
        registry(new ProjectStartByIdCommand());
        registry(new ProjectStartByIndexCommand());
        registry(new ProjectUpdateByIdCommand());
        registry(new ProjectUpdateByIndexCommand());

        registry(new TaskBindToProjectCommand());
        registry(new TaskChangeStatusByIdCommand());
        registry(new TaskChangeStatusByIndexCommand());
        registry(new TaskClearCommand());
        registry(new TaskCompleteByIdCommand());
        registry(new TaskCompleteByIndexCommand());
        registry(new TaskCreateCommand());
        registry(new TaskDisplayByIdCommand());
        registry(new TaskDisplayByIndexCommand());
        registry(new TaskDisplayByProjectId());
        registry(new TaskListCommand());
        registry(new TaskRemoveByIdCommand());
        registry(new TaskRemoveByIndexCommand());
        registry(new TaskStartByIdCommand());
        registry(new TaskStartByIndexCommand());
        registry(new TaskUnbindFromProjectCommand());
        registry(new TaskUpdateByIdCommand());
        registry(new TaskUpdateByIndexCommand());

        registry(new UserRegistryCommand());
        registry(new UserLoginCommand());
        registry(new UserLogoutCommand());
        registry(new UserChangePasswordCommand());
        registry(new UserUpdateProfileCommand());
        registry(new UserDisplayProfileCommand());
        registry(new UserLockCommand());
        registry(new UserUnlockCommand());
        registry(new UserRemoveCommand());
    }

    @Override
    @NotNull
    public ICommandService getCommandService() {
        return commandService;
    }

    @Override
    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @Override
    @NotNull
    public ILoggerService getLoggerService() {
        return loggerService;
    }

    @Override
    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    @Override
    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    private void initDemoData() {
        projectService.create("1", "TEST PROJECT", Status.IN_PROGRESS);
        projectService.create("1", "DEMO PROJECT", Status.COMPLETED);
        projectService.create("2", "BETA PROJECT", Status.NOT_STARTED);
        projectService.create("2", "BEST PROJECT", Status.IN_PROGRESS);
        taskService.create("1", "TEST TASK", Status.IN_PROGRESS);
        taskService.create("1", "DEMO TASK", Status.COMPLETED);
        taskService.create("2", "BETA TASK", Status.NOT_STARTED);
        taskService.create("2", "BEST TASK", Status.IN_PROGRESS);
        userService.create("user1", "user1", "user@mail.ru").setId("2");
        userService.create("akolobov", "akolobov", Role.ADMIN).setId("1");
    }

    public void run(@Nullable final String[] args) {
        processArguments(args);
        initDemoData();
        initLogger();
        processCommands();
    }

    private void initLogger() {
        loggerService.info("** WELCOME TO TASK MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void processArguments(@Nullable final String[] args) {
        if (args == null || args.length == 0) {
            return;
        }
        @Nullable final String arg = args[0];
        processArgument(arg);
        exit();
    }

    private void processCommands() {
        @NotNull final Scanner scanner = new Scanner(System.in);
        @Nullable String command = "";
        @Nullable AbstractCommand abstractCommand = null;
        while (!(abstractCommand instanceof ApplicationExitCommand)) {
            try {
                System.out.println("ENTER COMMAND:");
                command = scanner.nextLine();
                abstractCommand = processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.err.println("[FAIL]");
            }
        }
    }

    private AbstractCommand processCommand(@Nullable final String command) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByName(command);
        if (abstractCommand == null) throw new CommandNotSupportedException();
        authService.checkRoles(abstractCommand.getRoles());
        abstractCommand.execute();
        return abstractCommand;
    }

    private void processArgument(@Nullable final String arg) {
        @Nullable final AbstractCommand abstractCommand = commandService.getCommandByArgument(arg);
        if (abstractCommand == null) throw new ArgumentNotSupportedException();
        abstractCommand.execute();
    }

    private void registry(@NotNull final AbstractCommand command) {
        command.setServiceLocator(this);
        commandService.add(command);
    }

    private void exit() {
        System.exit(0);
    }

}
