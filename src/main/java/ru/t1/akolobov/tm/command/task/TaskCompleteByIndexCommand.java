package ru.t1.akolobov.tm.command.task;

import org.jetbrains.annotations.NotNull;
import ru.t1.akolobov.tm.enumerated.Status;
import ru.t1.akolobov.tm.util.TerminalUtil;

public final class TaskCompleteByIndexCommand extends AbstractTaskCommand {

    @NotNull
    public static final String NAME = "task-complete-by-index";

    @NotNull
    public static final String DESCRIPTION = "Complete task.";

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    public void execute() {
        System.out.println("[COMPLETE TASK BY INDEX]");
        System.out.println("ENTER INDEX:");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final String userId = getAuthService().getUserId();
        getTaskService().changeStatusByIndex(userId, index, Status.COMPLETED);
    }

}
