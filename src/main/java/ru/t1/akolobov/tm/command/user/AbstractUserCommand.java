package ru.t1.akolobov.tm.command.user;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.akolobov.tm.api.service.IAuthService;
import ru.t1.akolobov.tm.api.service.IUserService;
import ru.t1.akolobov.tm.command.AbstractCommand;
import ru.t1.akolobov.tm.exception.user.UserNotFoundException;
import ru.t1.akolobov.tm.model.User;

public abstract class AbstractUserCommand extends AbstractCommand {

    @NotNull
    protected IUserService getUserService() {
        return serviceLocator.getUserService();
    }

    @NotNull
    protected IAuthService getAuthService() {
        return serviceLocator.getAuthService();
    }

    @Override
    @Nullable
    public String getArgument() {
        return null;
    }

    protected void displayUser(@Nullable final User user) {
        if (user == null) throw new UserNotFoundException();
        System.out.println("ID: " + user.getId());
        System.out.println("LOGIN: " + user.getLogin());
        System.out.println("EMAIL: " + user.getEmail());
        System.out.println("ROLE: " + user.getRole().getDisplayName());
    }

}
